# Apprendre Kubernetes avec K3S

## Pré-requis

- VirtualBox (sur la machine hôte)

### Optionel

- Docker sur la machine hôte
- K9S sur la machine hôte
- Kubectl sur la machine hôte

## Installer un mono cluster K3S

- cloner le projet
- `cd learn-kube`
- `./create.sh`

Cela va créer une VM avec K3S, Kubectl, K9S et Helm. La VM partage son répertoire $HOME avec le projet, donc toutes les commandes `kubectl` peuvent être passées à l'intérieur de la VM ou à partir de la machine hôte si `kubectl` est installé dessus.

### Une fois le cluster installé

Il faut modifier le fichier `hosts` de la machine hôte et lui ajouter cette ligne: `172.16.245.230 k3s-cluster.test` 

> l'IP de la VM peut se modifier dans le fichier `Vagrantfile`

### Connexion SSH à la VM

Pour se connecter en ssh à la VM, il suffit de taper cette commande (dans le répertoire où est localisé le `Vagrantfile`):

```shell
vagrant ssh
```

le mot de passe est `vagrant`

### 1ers tests

> dans la VM ou à partir de la machine hôte:

```shell
export KUBECONFIG=.kube/config
kubectl top node
```

```shell
export KUBECONFIG=.kube/config
kubectl top pod -n kube-system
```

ou vous pouvez aussi lancer K9S:

```shell
export KUBECONFIG=.kube/config
k9s --all-namespaces 
```

## Pour déployer votre 1ère application

Aller voir par ici [01-first-steps/README.md](01-first-steps/README.md)