#!/bin/sh
vagrant halt; vagrant destroy -f
rm -rf .cache
rm -rf .gnupg
rm -rf .vagrant
rm -rf .local
rm -rf .config
rm -rf .k9s
rm -rf .kube
rm -rf bin
rm .bash_history
rm .sudo_as_admin_successful
rm -rf create_cluster
rm -rf srv
rm -rf faas-netes
rm -rf .docker

