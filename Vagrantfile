K3S_SERVER_NAME="k3s-mono-cluster-vm"
K3S_SERVER_DOMAIN = "k3s-cluster.test"
K3S_SERVER_IP="172.16.245.230"

BOX_IMAGE = "bento/ubuntu-18.04"

K9S_VERSION= "0.13.1"

Vagrant.configure("2") do |config|
  config.vm.box = BOX_IMAGE

  ENV['LC_ALL']="en_US.UTF-8"
  
  config.ssh.username = "vagrant"
  config.ssh.password = "vagrant"

  config.vm.define "#{K3S_SERVER_NAME}" do |node|
    node.vm.synced_folder "./", "/home/vagrant", id:"share"   
    node.vm.hostname = "#{K3S_SERVER_NAME}"
    
    # node.vm.network "public_network", ip:"#{SANDBOX_PUB_IP}", bridge: "en0: Wi-Fi (Wireless)"
    node.vm.network "forwarded_port", guest: 6443, host: 6443, host_ip: "0.0.0.0"
    
    node.vm.network "private_network", ip: "#{K3S_SERVER_IP}"

    node.vm.provider "virtualbox" do |vb|
      vb.memory = 8192
      vb.cpus = 4
      vb.name = "#{K3S_SERVER_NAME}"
    end

    # vagrant provision --provision-with install-docker
    node.vm.provision "install-docker", privileged: true, type: "shell" , inline: <<-SHELL
      echo "🐳 installing Docker"
      apt-get update
      # ----- Install Docker -----
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
      apt-key fingerprint 0EBFCD88
      add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
      apt-get update
      apt-get install -y docker-ce
      # -----------------------  
      echo "🐳 Docker install ✅"   
    SHELL

    node.vm.provision :shell, run: "always", inline: <<-SHELL
      sudo usermod -a -G docker $USER
      sudo chmod 666 /var/run/docker.sock      
    SHELL

    # vagrant provision --provision-with hosts-file
    node.vm.provision "hosts-file", privileged: true, type: "shell" , inline: <<-SHELL
      echo "📝 updating hosts file"
      # Add entries to hosts file:
      echo "" >> /etc/hosts
      echo '#{K3S_SERVER_IP} #{K3S_SERVER_DOMAIN}' >> /etc/hosts 
      echo "" >> /etc/hosts
    SHELL

    # vagrant provision --provision-with install-k3s
    node.vm.provision "install-k3s", privileged: false, type: "shell" , inline: <<-SHELL
      echo "🐳 installing K3S"
      #sudo modprobe vxlan #??? 🤔
      curl -sfL https://get.k3s.io | sh -
      #hostnamectl set-hostname #{K3S_SERVER_NAME}
      echo "🐳 K3S install ✅" 
    SHELL

    # vagrant provision --provision-with helm
    node.vm.provision "helm", privileged: false, type: "shell" , inline: <<-SHELL
      echo "🛠 installing helm"
      curl -L https://git.io/get_helm.sh | bash
      echo "🛠 helm install ✅" 
    SHELL

    # vagrant provision --provision-with k9s
    node.vm.provision "k9s", privileged: false, type: "shell" , inline: <<-SHELL
      export K9S_VERSION=#{K9S_VERSION}
      echo "🛠 installing K9S ${K9S_VERSION}"
      curl -LO https://github.com/derailed/k9s/releases/download/v0.13.1/k9s_${K9S_VERSION}_Linux_x86_64.tar.gz
      tar -xvzpf k9s_${K9S_VERSION}_Linux_x86_64.tar.gz
      chmod 775 k9s
      sudo mv k9s /usr/local/bin/
      rm k9s_${K9S_VERSION}_Linux_x86_64.tar.gz
      rm LICENSE
      rm README.md
      echo "🛠 K9S install ✅" 
    SHELL

    # vagrant provision --provision-with config
    node.vm.provision "config", privileged: false, type: "shell" , inline: <<-SHELL
      mkdir -p /home/vagrant/.kube 
      sudo cat /etc/rancher/k3s/k3s.yaml > /home/vagrant/.kube/config
    SHELL

    node.vm.provision :shell, run: "always", inline: <<-SHELL
      echo "🖐install kubectl and k9s on the host"
      echo "🖐🚧"

      echo "👋 on the host side:"
      echo "add this to your hosts file: #{K3S_SERVER_IP} #{K3S_SERVER_DOMAIN}"

      echo "👋 on the vm side:"
      echo "vagrant ssh"
      echo "export KUBECONFIG=.kube/config"
      echo "k9s --all-namespaces"
      echo "or run ./run-k9s.sh"
    SHELL

  end

end
