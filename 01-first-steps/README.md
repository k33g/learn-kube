# Demo: first-steps

Application Express à déployer sur K3S

## Déployer l'application

```shell
cd 01-first-steps
export KUBECONFIG=../.kube/config
export DOCKER_USER=<votre_handle_sur_dockerhub> 
export DOCKER_PASSWORD=<votre_password_sur_dockerhub>
export CLUSTER_IP="172.16.245.230"
./deploy.sh
```

## Supprimer l'application

```shell
cd 01-first-steps
export KUBECONFIG=../.kube/config
./remove.sh
```