#!/bin/sh

git add .
git commit -m "🎉 updated"

export TAG=$(git rev-parse --short HEAD)
export IMAGE_NAME="first_steps"
export APPLICATION_NAME="first-steps"
export HOST="${APPLICATION_NAME}.${CLUSTER_IP}.nip.io"

export CONTAINER_PORT=${CONTAINER_PORT:-3000}
export EXPOSED_PORT=${EXPOSED_PORT:-80}


export REPLICAS=${REPLICAS:-3}

echo ${DOCKER_PASSWORD} | docker login --username ${DOCKER_USER} --password-stdin

docker build -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME} ${DOCKER_USER}/${IMAGE_NAME}:${TAG}
docker push ${DOCKER_USER}/${IMAGE_NAME}:${TAG}

export user=${DOCKER_USER}

envsubst < ./deploy-template.yaml > ./kube/deploy-${TAG}.yaml

kubectl apply -f ./kube/deploy-${TAG}.yaml

echo "open http://${HOST}"
echo "or open http://${HOST}/api/hello"